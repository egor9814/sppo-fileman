/* auto-generated file */
#include <resources/R>
#include <memory>

struct Holder {
    QMap<QString, std::shared_ptr<R::string::__lang__>> map;
    Holder();
    static Holder& getInstance() { static Holder h; return h; }
    static R::string::__lang__& getLang(const QString& lang) {
        auto &map = getInstance().map;
        if (map.count(lang)) return *(map[lang]);
        return *(map["en"]);
    }
};

const QString& R::string::analyzing_dir(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).analyzing_dir();
}

const QString& R::string::current_path(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).current_path();
}

const QString& R::string::current_path_none(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).current_path_none();
}

const QString& R::string::dir_current(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).dir_current();
}

const QString& R::string::dir_no_ext(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).dir_no_ext();
}

const QString& R::string::dir_other(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).dir_other();
}

const QString& R::string::empty_folder(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).empty_folder();
}

const QString& R::string::help_about_text(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).help_about_text();
}

const QString& R::string::help_about_title(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).help_about_title();
}

const QString& R::string::menu_dss(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_dss();
}

const QString& R::string::menu_dss_files(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_dss_files();
}

const QString& R::string::menu_dss_folders(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_dss_folders();
}

const QString& R::string::menu_file(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_file();
}

const QString& R::string::menu_help(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_help();
}

const QString& R::string::menu_help_about(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_help_about();
}

const QString& R::string::menu_help_qt(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_help_qt();
}

const QString& R::string::menu_lang(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_lang();
}

const QString& R::string::menu_lang_en(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_lang_en();
}

const QString& R::string::menu_lang_ru(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_lang_ru();
}

const QString& R::string::menu_legend(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_legend();
}

const QString& R::string::menu_legend_bottom(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_legend_bottom();
}

const QString& R::string::menu_legend_left(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_legend_left();
}

const QString& R::string::menu_legend_not(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_legend_not();
}

const QString& R::string::menu_legend_right(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_legend_right();
}

const QString& R::string::menu_legend_top(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_legend_top();
}

const QString& R::string::menu_reanalyze(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_reanalyze();
}

const QString& R::string::menu_view_type(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_view_type();
}

const QString& R::string::menu_view_type_bar(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_view_type_bar();
}

const QString& R::string::menu_view_type_list(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_view_type_list();
}

const QString& R::string::menu_view_type_pie(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).menu_view_type_pie();
}

const QString& R::string::name(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).name();
}

const QString& R::string::please_wait(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).please_wait();
}

const QString& R::string::relation(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).relation();
}

const QString& R::string::size(const QLocale& loc) {
    auto lang = QLocale::languageToString(loc.language()).left(2).toLower();
    return Holder::getLang(lang).size();
}

Holder::Holder() {
    map.insert("en", std::make_shared<R::string::en>());
    map.insert("ru", std::make_shared<R::string::ru>());
}

