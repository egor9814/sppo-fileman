macro(init_R sources)
    include_directories(res/bin/include)
    list(APPEND ${sources} res/bin/R.cpp)
    list(APPEND ${sources} res/bin/include/resources/R)
endmacro(init_R)
