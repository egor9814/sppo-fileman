//
// Created by egor9814 on 26.04.19.
//

#ifndef __egor9814_fileman__main_hpp__
#define __egor9814_fileman__main_hpp__

#include <QtWidgets/QApplication>
#include <QtCore/QCoreApplication>

#ifdef USE_CONSOLE_MODE
#define NEW_APP_INSTANCE(name, argc, argv) QCoreApplication name(argc, argv)
#define EXEC_APP() QCoreApplication::exec()
#else
#define NEW_APP_INSTANCE(name, argc, argv) QApplication name(argc, argv)
#define EXEC_APP() QApplication::exec()
#endif

#endif //__egor9814_fileman__main_hpp__
