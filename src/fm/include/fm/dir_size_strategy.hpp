//
// Created by egor9814 on 26.04.19.
//

#ifndef __egor9814_fileman__dir_size_strategy_hpp__
#define __egor9814_fileman__dir_size_strategy_hpp__

#include <fm/file_util.hpp>
#include <fm/shared_preferences.hpp>
#include <map>
#include <QtCore/QtCore>

namespace fm {

    struct DirSizeStrategy {
        using DirStateType = QMap<QString, QPair<qint64, qreal>>;

        static DirSizeStrategy* byFolder();

        static DirSizeStrategy* byFiles();

        DirStateType calculateSizes(const util::File& rootPath);

        virtual void resetState() {}

        /*virtual void saveState(util::SharedPreferences& preferences) {}

        virtual void restoreState(util::SharedPreferences& preferences) {}*/

    protected:
        virtual void calc(const util::File& path,
                          QMap<QString, qint64>& result) = 0;
    };

}

#endif //__egor9814_fileman__dir_size_strategy_hpp__
