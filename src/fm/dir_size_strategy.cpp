//
// Created by egor9814 on 26.04.19.
//

#include <fm/dir_size_strategy.hpp>
#include <iostream>
#include <fm/time.hpp>
#include <QtCore/QDebug>

struct DSS_ByFolder : public fm::DirSizeStrategy {
private:
    static QMap<QString, qint64>& cache() {
        static QMap<QString, qint64> map;
        return map;
    }

public:
    void resetState() override {
        cache().clear();
    }

    /*void saveState(fm::util::SharedPreferences &preferences) override {
        auto &editor = preferences.edit().clear();
        for (auto &it : cache()) {
            editor.putLong(it.first, it.second);
        }
        editor.apply();
    }

    void restoreState(fm::util::SharedPreferences &preferences) override {
        for (auto &it : preferences.getAll()) {
            cache()[it.first] = it.second.value<qint64>();
        }
    }*/

protected:
    void calc(const fm::util::File &path,
              QMap<QString, qint64> &result) override {
        auto &cache = DSS_ByFolder::cache();

        for (auto &f : path.getFiles()) {
            if (f.isDir()) {
                auto p = f.getAbsoluteFilePath();
                if (cache.count(p)) {
                    result[f.getFileName()] = cache[p];
                } else {
                    QMap<QString, qint64> subdir;
                    calc(f, subdir);
                    auto &sum = result[f.getFileName()];
                    for (auto &it : subdir) {
                        sum += it;
                    }
                    cache[p] = sum;
                }
            } else {
                result["."] += f.getFileSize();
            }
        }
    }
};

struct DSS_ByFiles : public fm::DirSizeStrategy {
protected:
    void calc(const fm::util::File &path,
              QMap<QString, qint64> &result) override {
        for (auto &f : path.getFiles()) {
            if (f.isDir()) {
                calc(f, result);
            } else {
                result[f.getFileExtension()] += f.getFileSize();
            }
        }
    }
};


fm::DirSizeStrategy::DirStateType fm::DirSizeStrategy::calculateSizes(const fm::util::File &rootPath) {
    auto startTime = time::systemMillis();
    QMap<QString, qint64> dir;
    calc(rootPath, dir);

    DirStateType result;
    if (dir.empty()) {
        auto t = time::systemMillis() - startTime;
        qDebug() << "DSS: " << rootPath.getAbsoluteFilePath() << " -> " << t.count() << "ms\n";
        return std::move(result);
    }

    qint64 all = 0;
    for (auto &it : dir.toStdMap()) {
        result[it.first] = {it.second, 0};
        all += it.second;
    }
    if (all != 0) {
        for (auto &it : result) {
            it.second = it.first * 100.0 / all;
        }
    }

    DirStateType optimized;
    qint64 other = 0;
    for (auto &it : result.toStdMap()) {
        if (it.second.second <= 1.0) {
            other += it.second.first;
        } else {
            optimized.insert(it.first, it.second);
        }
    }
    if (other != 0) {
        optimized["Other"] = QPair<qint64, qreal>(other, 100.0 * other / all);
    }

    auto t = time::systemMillis() - startTime;
    qDebug() << "DSS: " << rootPath.getAbsoluteFilePath() << " -> " << t.count() << "ms\n";
    return std::move(optimized);
}


fm::DirSizeStrategy *fm::DirSizeStrategy::byFolder() {
    static DSS_ByFolder instance;
    return &instance;
}

fm::DirSizeStrategy *fm::DirSizeStrategy::byFiles() {
    static DSS_ByFiles instance;
    return &instance;
}
