//
// Created by egor9814 on 03.05.19.
//

#include "data_loader.hpp"
#include <functional>
#include <QtCore/QtCore>
#include "_private/data_loader_private.hpp"
#include <resources/R>

fm::ui::DataLoadingDialog::DataLoadingDialog(QLocale& lang, QWidget *parent)
    : QObject(parent), parent(parent), lang(lang) {}

fm::ui::DataLoadingDialog::~DataLoadingDialog() {}

void fm::ui::DataLoadingDialog::load(fm::DirSizeStrategy *dss, const QString &path) {
    using namespace data_loader;

    auto loader = new Loader(dss, path);
    auto thread = new QThread;
    loader->moveToThread(thread);

    auto dialog = new QMessageBox(parent);
    dialog->setWindowTitle(R::string::please_wait(lang));
    dialog->setText(R::string::analyzing_dir(lang).arg(path));
    auto ok = dialog->addButton(QMessageBox::Ok);
    ok->setEnabled(false);

    QObject::connect(thread, &QThread::started, loader, &Loader::load);
    QObject::connect(loader, &Loader::finished, thread, &QThread::quit);
    QObject::connect(loader, &Loader::finished, loader, &Loader::deleteLater);
    QObject::connect(thread, &QThread::finished, thread, &QThread::deleteLater);
    QObject::connect(thread, &QThread::finished, dialog, &QMessageBox::accept);
    QObject::connect(thread, &QThread::finished, dialog, &QMessageBox::deleteLater);
    QObject::connect(loader, &Loader::loaded, this, &DataLoadingDialog::loadFinished);

    thread->start();
    dialog->exec();
}

void fm::ui::DataLoadingDialog::loadFinished(const DirSizeStrategy::DirStateType & map) {
    emit loaded(map);
}

/*void fm::ui::DataLoadingDialog::loadCanceled() {
    emit loaded({});
}*/
