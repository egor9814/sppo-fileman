//
// Created by egor9814 on 02.05.19.
//

#include "file_list.hpp"
#include "_private/file_list_private.hpp"
#include <fm/file_util.hpp>
#include <fm/dir_size_strategy.hpp>
#include <resources/R>

/// Data model
class DataModel : public QAbstractTableModel {
    struct Row {
        QString fileName;
        QString size;
        QString rel;

        const QString&operator[](unsigned int col) const {
            static QString empty;
            switch (col) {
                case 0:
                    return fileName;

                case 1:
                    return size;

                case 2:
                    return rel;

                default:
                    return empty;
            }
        }
    };

    QList<Row> rows;

    QLocale& lang;

public:
    explicit DataModel(QLocale& lang, const fm::DirSizeStrategy::DirStateType &data,
            QObject *parent = nullptr)
            : QAbstractTableModel(parent), lang(lang) {
        for (auto &it : data.toStdMap()) {
            rows << Row { it.first,
                          fm::util::FileSizeUnit(it.second.first).autoDetect().toString(),
                          QString::number(it.second.second, 'f', 3)
            };
        }
    }

    int rowCount(const QModelIndex &parent) const override {
        return rows.size();
    }

    int columnCount(const QModelIndex &parent) const override {
        return 3;
    }

    QVariant data(const QModelIndex &index, int role) const override {
        if (index.isValid() && index.row() < rows.size() && role == Qt::DisplayRole) {
            return QVariant(rows[index.row()][index.column()]);
        }
        return QVariant();
    }

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override {
        if (role == Qt::DisplayRole) {
            if (orientation == Qt::Horizontal) {
                switch (section) {
                    default:
                        break;

                    case 0: {
                        static QVariant fileName(R::string::name(lang));
                        return fileName;
                    }

                    case 1: {
                        static QVariant fileSize(R::string::size(lang));
                        return fileSize;
                    }

                    case 2: {
                        static QVariant fileRel(R::string::relation(lang));
                        return fileRel;
                    }
                }
            } else {
                return QVariant(QString::number(section + 1));
            }
        }

        return QVariant();
    }
};


/// View impl
fm::ui::data_view::FileListView::FileListView(QLocale& lang) : lang(lang) {
    table = new QTableView();
    table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    table->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
}

fm::ui::data_view::FileListView::~FileListView() {
    delete table;
}

QWidget *fm::ui::data_view::FileListView::getWidget() {
    return table;
}

void fm::ui::data_view::FileListView::setData(const DirSizeStrategy::DirStateType &data) {
    table->setModel(new DataModel(lang, data, table));
    table->sortByColumn(2);
}

fm::ui::DataViewType fm::ui::data_view::FileListView::getType() const {
    return fm::ui::DataViewType::FileList;
}
