//
// Created by egor9814 on 02.05.19.
//

#ifndef __egor9814_fileman__data_view_diagram_hpp__
#define __egor9814_fileman__data_view_diagram_hpp__

#include <QtWidgets/QtWidgets>
#include <QtCharts/QtCharts>
#include "../data_view.hpp"

namespace fm {

    namespace ui {

        namespace data_view {

            class Diagram : public DataGraphicsProvider {
                QChartView* chartView;
                QLocale& lang;

            public:
                Diagram(QLocale& lang);
                ~Diagram();

                QWidget *getWidget() override;

                void showLegend(Qt::Alignment alignment) override;

                void hideLegend() override;

                void setData(const DirSizeStrategy::DirStateType &data) override;

                QLocale& getLang();
            protected:
                virtual void setupSeries(const DirSizeStrategy::DirStateType &data, QChart *chart) = 0;

                virtual QChart::AnimationOptions getChartAnimationOptions() const;

                virtual QEasingCurve getChartAnimationEasingCurve() const;

                virtual QChart::ChartTheme getChartTheme() const;
            };

        }

    }

}

#endif //__egor9814_fileman__data_view_diagram_hpp__
