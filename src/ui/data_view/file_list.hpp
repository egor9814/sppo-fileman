//
// Created by egor9814 on 02.05.19.
//

#ifndef __egor9814_fileman__data_view_file_list_hpp__
#define __egor9814_fileman__data_view_file_list_hpp__

#include <QtWidgets/QtWidgets>
#include "../data_view.hpp"

namespace fm {

    namespace ui {

        namespace data_view {
            #include "_private/file_list_private.hpp"
        }

    }

}

#endif //__egor9814_fileman__data_view_file_list_hpp__
