//
// Created by egor9814 on 02.05.19.
//

#include "bar_diagram.hpp"
#include "_private/bar_diagram_private.hpp"

fm::ui::data_view::BarDiagramView::BarDiagramView(QLocale& lang) : Diagram(lang) {}

fm::ui::data_view::BarDiagramView::~BarDiagramView() = default;

fm::ui::DataViewType fm::ui::data_view::BarDiagramView::getType() const {
    return DataViewType::BarsDiagram;
}

void fm::ui::data_view::BarDiagramView::setupSeries(const DirSizeStrategy::DirStateType &data, QChart *chart) {
    auto series = new QBarSeries(chart);
    for (auto &it : data.toStdMap()) {
        auto rel = it.second.second;
        auto set = new QBarSet(it.first + ": " + QString::number(rel, 'f', 3) + "%", series);
        set->append(rel);
        series->append(set);
    }
    chart->addSeries(series);

    /*auto x = new QBarCategoryAxis();
    for (auto &it : data) {
        x->append(it.first);
    }
    chart->addAxis(x, Qt::AlignBottom);
    series->attachAxis(x);*/

    auto y = new QValueAxis();
    y->setRange(0, 100);
    chart->addAxis(y, Qt::AlignLeft);
    series->attachAxis(y);
}

QEasingCurve fm::ui::data_view::BarDiagramView::getChartAnimationEasingCurve() const {
    return QEasingCurve(QEasingCurve::OutElastic);
}
