//
// Created by egor9814 on 02.05.19.
//

#ifndef __egor9814_fileman__data_view_file_list_private_hpp__
#define __egor9814_fileman__data_view_file_list_private_hpp__

class FileListView : public fm::ui::DataGraphicsProvider {
    QTableView* table;
    QLocale& lang;

public:
    FileListView(QLocale& lang);

    ~FileListView() override;

    QWidget *getWidget() override;

    void setData(const DirSizeStrategy::DirStateType &data) override;

    void showLegend(Qt::Alignment) override {}

    void hideLegend() override {}

    fm::ui::DataViewType getType() const override;
};

#endif //__egor9814_fileman__data_view_file_list_private_hpp__
