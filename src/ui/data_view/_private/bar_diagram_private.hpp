//
// Created by egor9814 on 02.05.19.
//

#ifndef __egor9814_fileman__data_view_bar_diagram_private_hpp__
#define __egor9814_fileman__data_view_bar_diagram_private_hpp__

class BarDiagramView : public Diagram {
public:
    BarDiagramView(QLocale& lang);

    ~BarDiagramView();

    DataViewType getType() const override;

protected:
    void setupSeries(const DirSizeStrategy::DirStateType &data, QChart *chart) override;

    QEasingCurve getChartAnimationEasingCurve() const override;
};

#endif //__egor9814_fileman__data_view_bar_diagram_private_hpp__
