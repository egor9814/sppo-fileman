//
// Created by egor9814 on 02.05.19.
//

#ifndef __egor9814_fileman__data_view_nothing_private_hpp__
#define __egor9814_fileman__data_view_nothing_private_hpp__

class NothingView : public fm::ui::DataGraphicsProvider {
    QLabel* message;

public:
    NothingView(QLocale& lang);

    ~NothingView() override;

    QWidget *getWidget() override;

    void setData(const DirSizeStrategy::DirStateType &data) override {}

    void showLegend(Qt::Alignment) override {}

    void hideLegend() override {}

    fm::ui::DataViewType getType() const override;
};

#endif //__egor9814_fileman__data_view_nothing_private_hpp__
