//
// Created by egor9814 on 02.05.19.
//

#include "nothing.hpp"
#include "_private/nothing_private.hpp"
#include <resources/R>


fm::ui::data_view::NothingView::NothingView(QLocale& lang)  {
    message = new QLabel(R::string::empty_folder(lang));
    message->setAlignment(Qt::AlignCenter);
}

fm::ui::data_view::NothingView::~NothingView() {
    delete message;
}

QWidget *fm::ui::data_view::NothingView::getWidget() {
    return message;
}

fm::ui::DataViewType fm::ui::data_view::NothingView::getType() const {
    return fm::ui::DataViewType::Nothing;
}
