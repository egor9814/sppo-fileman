//
// Created by egor9814 on 02.05.19.
//

#include "pie_diagram.hpp"
#include "_private/pie_diagram_private.hpp"

fm::ui::DataViewType fm::ui::data_view::PieDiagramView::getType() const {
    return DataViewType::PieDiagram;
}

void fm::ui::data_view::PieDiagramView::setupSeries(const DirSizeStrategy::DirStateType &data, QChart *chart) {
    auto series = new QPieSeries(chart);
    if (!data.empty()) {
        for (auto &it : data.toStdMap()) {
            auto rel = it.second.second;
            auto slice = series->append(it.first + ": " + QString::number(rel, 'f', 3) + "%", rel);
            slice->setLabelVisible();
        }
    }
    series->setPieSize(0.75);
    chart->addSeries(series);
}

QEasingCurve fm::ui::data_view::PieDiagramView::getChartAnimationEasingCurve() const {
    return QEasingCurve(QEasingCurve::InOutCubic);
}
