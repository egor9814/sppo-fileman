//
// Created by egor9814 on 02.05.19.
//

#ifndef __egor9814_fileman__data_view_nothing_hpp__
#define __egor9814_fileman__data_view_nothing_hpp__

#include <QtWidgets/QtWidgets>
#include "../data_view.hpp"

namespace fm {

    namespace ui {

        namespace data_view {
            #include "_private/nothing_private.hpp"
        }

    }

}

#endif //__egor9814_fileman__data_view_nothing_hpp__
