//
// Created by egor9814 on 02.05.19.
//

#ifndef __egor9814_fileman__data_view_pie_diagram_hpp__
#define __egor9814_fileman__data_view_pie_diagram_hpp__

#include "diagram.hpp"

namespace fm {

    namespace ui {

        namespace data_view {
            #include "_private/pie_diagram_private.hpp"
        }

    }

}

#endif //__egor9814_fileman__data_view_pie_diagram_hpp__
