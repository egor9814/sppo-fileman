//
// Created by egor9814 on 02.05.19.
//

#include "diagram.hpp"

fm::ui::data_view::Diagram::Diagram(QLocale& lang) : lang(lang) {
    chartView = new QChartView();
    chartView->setRenderHint(QPainter::Antialiasing, true);
}

fm::ui::data_view::Diagram::~Diagram() {
    delete chartView;
}

QWidget *fm::ui::data_view::Diagram::getWidget() {
    return chartView;
}

void fm::ui::data_view::Diagram::showLegend(Qt::Alignment alignment) {
    auto chart = chartView->chart();
    if (chart) {
        chart->legend()->setAlignment(alignment);
        chart->legend()->show();
    }
}

void fm::ui::data_view::Diagram::hideLegend() {
    auto chart = chartView->chart();
    if (chart) {
        chart->legend()->hide();
    }
}

void fm::ui::data_view::Diagram::setData(const DirSizeStrategy::DirStateType &data) {
    auto chart = new QChart();
    setupSeries(data, chart);
    chartView->setChart(chart);
    chart->setAnimationOptions(getChartAnimationOptions());
    chart->setAnimationEasingCurve(getChartAnimationEasingCurve());
    chart->setTheme(getChartTheme());
}

QLocale &fm::ui::data_view::Diagram::getLang() {
    return lang;
}

QChart::AnimationOptions fm::ui::data_view::Diagram::getChartAnimationOptions() const {
    return QChart::SeriesAnimations;
}

QEasingCurve fm::ui::data_view::Diagram::getChartAnimationEasingCurve() const {
    return QEasingCurve(QEasingCurve::Linear);
}

QChart::ChartTheme fm::ui::data_view::Diagram::getChartTheme() const {
    return QChart::ChartThemeDark;
}
