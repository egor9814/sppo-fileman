//
// Created by egor9814 on 26.04.19.
//

#include "main_window.hpp"
#include <QtWidgets/QApplication>
#include <fm/dir_size_strategy.hpp>
#include <fm/launch_state.hpp>
#include <resources/R>

class MyFileSystemModel : public QFileSystemModel {
    QLocale& lang;
public:
    explicit MyFileSystemModel(QLocale& lang, QObject *parent = nullptr)
        : QFileSystemModel(parent), lang(lang) {}

    int columnCount(const QModelIndex &parent) const override {
        return 1;
    }

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override {
        if (role == Qt::DisplayRole) {
            if (orientation == Qt::Horizontal) {
                switch (section) {
                    default:
                        break;

                    case 0: {
                        static QVariant fileName(R::string::name(lang));
                        return fileName;
                    }
                }
            }
        }

        return QFileSystemModel::headerData(section, orientation, role);
    }
};


/*template <typename T>
void themedPalette(T* object) {
    auto palette = object->palette();
    palette.setColor(QPalette::Window, QRgb(0x121218));
    palette.setColor(QPalette::Light, QRgb(0x121218));
    palette.setColor(QPalette::WindowText, QRgb(0xd6d6d6));
    object->setPalette(palette);
}*/

fm::ui::MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), lang(QLocale::English) {
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    setMinimumSize(0, 0);
    resize(600, 480);
    setWindowTitle(QString("File Man"));
    statusBar()->showMessage(R::string::current_path().arg(R::string::current_path_none()));

    //themedPalette(window());

    dataLoader = new DataLoadingDialog(lang, this);
    connect(dataLoader, &DataLoadingDialog::loaded, this, &MainWindow::showDirStatus);

    prefs = std::shared_ptr<fm::util::SharedPreferences>(
            fm::util::SharedPreferences::defaultPreferences("ru.egor9814.fileman"));
    auto langPref = static_cast<QLocale::Language>(prefs->getInt("lang",
            static_cast<qint32>(QLocale::English)));
    if (lang.language() != langPref) {
        lang = QLocale(langPref);
    }

    dirSizeStrategy = DirSizeStrategy::byFolder();

    mainSplitter = new QSplitter(this);
    mainSplitter->setObjectName(QString("mainSplitter"));

    fileTree = new QTreeView(mainSplitter);
    fileTree->setObjectName(QString("fileTree"));
    fileTree->setSortingEnabled(true);
    fileTree->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    fileTree->header()->setStretchLastSection(false);
    fileTree->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
    fileTreeModel = new MyFileSystemModel(lang, fileTree);
    fileTreeModel->setObjectName("fileTreeModel");
    fileTreeModel->setFilter(QDir::AllDirs | QDir::NoDotAndDotDot
                                | QDir::Hidden | QDir::System | QDir::NoSymLinks);
    auto rootPath = (util::File::home() / "test").getAbsoluteFilePath();
    fileTreeModel->setRootPath(util::File::root().getAbsoluteFilePath());
    currentPath = fileTreeModel->index(rootPath);
    fileTree->setModel(fileTreeModel);
    fileTree->expandToDepth(1);
    fileTreeModel->sort(0, Qt::SortOrder::AscendingOrder);
    connect(fileTree->selectionModel(), &QItemSelectionModel::currentChanged, this, &MainWindow::onDirSelected);
    //themedPalette(fileTree);

    data = new DataView(lang, this);
    data->setObjectName("data");
    //themedPalette(data);

    mainSplitter->addWidget(fileTree);
    mainSplitter->addWidget(data);

    //mainSplitter->setStretchFactor(0, 5);
    //mainSplitter->setStretchFactor(1, 5);

    setCentralWidget(mainSplitter);


    exitShortcut = new QShortcut(this);
    exitShortcut->setKey(Qt::Key_F10);
    connect(exitShortcut, SIGNAL(activated()), this, SLOT(exitShortcutSlot()));

    fsShortcut = new QShortcut(this);
    fsShortcut->setKey(Qt::Key_F11);
    connect(fsShortcut, SIGNAL(activated()), this, SLOT(fsShortcutSlot()));


    /// File operations
    auto menu = new QMenu(R::string::menu_file(lang), menuBar());
    menuBar()->addMenu(menu);

    auto actionReanalyze = menu->addAction(R::string::menu_reanalyze(lang));
    connect(actionReanalyze, &QAction::triggered, this, &MainWindow::onReanalyzeAction);

    /// Language
    menu = new QMenu(R::string::menu_lang(lang), menuBar());
    menuBar()->addMenu(menu);

    auto actionLanguageList = new QActionGroup(menu);
    connect(actionLanguageList->addAction(R::string::menu_lang_en(lang)), &QAction::triggered,
            this, &MainWindow::onSelectLangEn);
    connect(actionLanguageList->addAction(R::string::menu_lang_ru(lang)), &QAction::triggered,
            this, &MainWindow::onSelectLangRu);

    menu->addActions(actionLanguageList->actions());

    /// Size strategy
    menu = new QMenu(R::string::menu_dss(lang), menuBar());
    menuBar()->addMenu(menu);

    auto actionDssTypeList = new QActionGroup(menu/*actionDssType*/);

    /// SS by Folder
    auto actionDssFolder = actionDssTypeList->addAction(R::string::menu_dss_folders(lang));
    actionDssFolder->setCheckable(true);
    connect(actionDssFolder, &QAction::toggled, this, &MainWindow::onDssFolderToggled);

    /// SS by Files
    auto actionDssFile = actionDssTypeList->addAction(R::string::menu_dss_files(lang));
    actionDssFile->setCheckable(true);
    connect(actionDssFile, &QAction::toggled, this, &MainWindow::onDssFileToggled);

    menu->addActions(actionDssTypeList->actions());

    /// View type
    menu = new QMenu(R::string::menu_view_type(lang), menuBar());
    menuBar()->addMenu(menu);
    auto actionViewTypeList = new QActionGroup(menu);

    /// VT: File list
    auto actionViewTypeFileList = actionViewTypeList->addAction(R::string::menu_view_type_list(lang));
    actionViewTypeFileList->setCheckable(true);
    connect(actionViewTypeFileList, &QAction::toggled, this, &MainWindow::onViewTypeFileListToggled);

    /// VT: Pie diagram
    auto actionViewTypePieDiagram = actionViewTypeList->addAction(R::string::menu_view_type_pie(lang));
    actionViewTypePieDiagram->setCheckable(true);
    connect(actionViewTypePieDiagram, &QAction::toggled, this, &MainWindow::onViewTypePieDiagramToggled);

    /// VT: Bar diagram
    auto actionViewTypeBarDiagram = actionViewTypeList->addAction(R::string::menu_view_type_bar(lang));
    actionViewTypeBarDiagram->setCheckable(true);
    connect(actionViewTypeBarDiagram, &QAction::toggled, this, &MainWindow::onViewTypeBarsToggled);

    menu->addActions(actionViewTypeList->actions());


    /// Legend visible
    menu = new QMenu(R::string::menu_legend(lang), menuBar());
    menuBar()->addMenu(menu);
    auto actionLegendList = new QActionGroup(menu);

    /// LV: None
    auto actionLegendNone = actionLegendList->addAction(R::string::menu_legend_not(lang));
    actionLegendNone->setCheckable(true);
    connect(actionLegendNone, &QAction::toggled, this, &MainWindow::onLegendNothingToggled);

    /// LV: Left
    auto actionLegendLeft = actionLegendList->addAction(R::string::menu_legend_left(lang));
    actionLegendLeft->setCheckable(true);
    connect(actionLegendLeft, &QAction::toggled, this, &MainWindow::onLegendLeftToggled);

    /// LV: Top
    auto actionLegendTop = actionLegendList->addAction(R::string::menu_legend_top(lang));
    actionLegendTop->setCheckable(true);
    connect(actionLegendTop, &QAction::toggled, this, &MainWindow::onLegendTopToggled);

    /// LV: Right
    auto actionLegendRight = actionLegendList->addAction(R::string::menu_legend_right(lang));
    actionLegendRight->setCheckable(true);
    connect(actionLegendRight, &QAction::toggled, this, &MainWindow::onLegendRightToggled);

    /// LV: Bottom
    auto actionLegendBottom = actionLegendList->addAction(R::string::menu_legend_bottom(lang));
    actionLegendBottom->setCheckable(true);
    connect(actionLegendBottom, &QAction::toggled, this, &MainWindow::onLegendBottomToggled);

    menu->addActions(actionLegendList->actions());

    /// Help
    menu = new QMenu(R::string::menu_help(lang));
    menuBar()->addMenu(menu);

    auto actionAbout = menu->addAction(R::string::menu_help_about(lang));
    connect(actionAbout, &QAction::triggered, this, &MainWindow::onAboutAction);

    auto actionAboutQt = menu->addAction(R::string::menu_help_qt(lang));
    connect(actionAboutQt, &QAction::triggered, this, &MainWindow::onAboutQtAction);


    /// Preferences loading
    auto dssPref = prefs->getInt("dss");
    auto vtPref = prefs->getInt("vt");
    auto lvPref = prefs->getInt("lv");

    switch (dssPref) {
        default:
        case 0:
            actionDssFolder->setChecked(true);
            break;

        case 1:
            dirSizeStrategy = DirSizeStrategy::byFiles();
            actionDssFile->setChecked(true);
            break;
    }

    switch (vtPref) {
        default:
        case 0:
            currentDataViewType = DataViewType::FileList;
            actionViewTypeFileList->setChecked(true);
            break;

        case 1:
            currentDataViewType = DataViewType::PieDiagram;
            actionViewTypePieDiagram->setChecked(true);
            break;

        case 2:
            currentDataViewType = DataViewType::BarsDiagram;
            actionViewTypeBarDiagram->setChecked(true);
            break;
    }
    data->setViewType(currentDataViewType);

    switch (lvPref) {
        default:
        case 0:
            actionLegendNone->setChecked(true);
            break;

        case 1:
            data->showLegend(Qt::AlignLeft);
            actionLegendLeft->setChecked(true);
            break;

        case 2:
            data->showLegend(Qt::AlignTop);
            actionLegendTop->setChecked(true);
            break;

        case 3:
            data->showLegend(Qt::AlignRight);
            actionLegendRight->setChecked(true);
            break;

        case 4:
            data->showLegend(Qt::AlignBottom);
            actionLegendBottom->setChecked(true);
            break;
    }

    //restoreState();
    firstOnce = false;
    //onDirSelected(currentPath, currentPath);
    auto startPath = util::LaunchState::get().startPath;
    if (!startPath.empty())
        currentPath = fileTreeModel->index(QString(startPath.c_str()));
    fileTree->setCurrentIndex(currentPath);
    fileTree->scrollTo(currentPath, QTreeView::PositionAtCenter);
}

fm::ui::MainWindow::~MainWindow() {
    //saveState();
    delete fileTree;
    //delete fileTreeModel;
    delete data;
    delete mainSplitter;
    delete exitShortcut;
    delete fsShortcut;
}

void fm::ui::MainWindow::exitShortcutSlot() {
    QApplication::quit();
}

void fm::ui::MainWindow::fsShortcutSlot() {
    if (isFullScreen()) {
        showNormal();
    } else {
        showFullScreen();
    }
}

void fm::ui::MainWindow::onDirSelected(const QModelIndex &selected, const QModelIndex &deselected) {
    if (firstOnce) {
        return;
    }
    currentPath = selected;
    auto fs = reinterpret_cast<const QFileSystemModel*>(selected.model());
    auto path = fs->filePath(selected);
    statusBar()->showMessage(R::string::current_path(lang).arg(path));
    //auto result = dirSizeStrategy->calculateSizes(util::File(path));
    dataLoader->load(dirSizeStrategy, path);
}

void fm::ui::MainWindow::updateView() {
    if (firstOnce)
        return;
    //auto currentPath = fileTree->currentIndex();
    onDirSelected(currentPath, currentPath);
    //fileTree->setCurrentIndex(currentPath);
}

void fm::ui::MainWindow::changeDss(bool isFolder) {
    if (isFolderDss != isFolder) {
        isFolderDss = isFolder;
        //dirSizeStrategy->resetState();
        dirSizeStrategy = isFolder ? DirSizeStrategy::byFolder() : DirSizeStrategy::byFiles();

        updateView();
    }
}

void fm::ui::MainWindow::onDssFolderToggled(bool checked) {
    if (checked) {
        changeDss(true);
        prefs->edit().putInt("dss", 0).apply();
    }
}

void fm::ui::MainWindow::onDssFileToggled(bool checked) {
    if (checked) {
        changeDss(false);
        prefs->edit().putInt("dss", 1).apply();
    }
}

void fm::ui::MainWindow::changeDataView(const fm::ui::DataViewType &type) {
    data->setViewType(type);
    //updateView();
}

void fm::ui::MainWindow::onViewTypeFileListToggled(bool checked) {
    if (checked) {
        changeDataView(DataViewType::FileList);
        prefs->edit().putInt("vt", 0).apply();
    }
}

void fm::ui::MainWindow::onViewTypePieDiagramToggled(bool checked) {
    if (checked) {
        changeDataView(DataViewType::PieDiagram);
        prefs->edit().putInt("vt", 1).apply();
    }
}

void fm::ui::MainWindow::onViewTypeBarsToggled(bool checked) {
    if (checked) {
        changeDataView(DataViewType::BarsDiagram);
        prefs->edit().putInt("vt", 2).apply();
    }
}

void fm::ui::MainWindow::onLegendNothingToggled(bool checked) {
    if (checked) {
        data->hideLegend();
        prefs->edit().putInt("lv", 0).apply();
    }
}

void fm::ui::MainWindow::onLegendLeftToggled(bool checked) {
    if (checked) {
        data->showLegend(Qt::AlignLeft);
        prefs->edit().putInt("lv", 1).apply();
    }
}

void fm::ui::MainWindow::onLegendTopToggled(bool checked) {
    if (checked) {
        data->showLegend(Qt::AlignTop);
        prefs->edit().putInt("lv", 2).apply();
    }
}

void fm::ui::MainWindow::onLegendRightToggled(bool checked) {
    if (checked) {
        data->showLegend(Qt::AlignRight);
        prefs->edit().putInt("lv", 3).apply();
    }
}

void fm::ui::MainWindow::onLegendBottomToggled(bool checked) {
    if (checked) {
        data->showLegend(Qt::AlignBottom);
        prefs->edit().putInt("lv", 4).apply();
    }
}

void fm::ui::MainWindow::onReanalyzeAction(bool) {
    dirSizeStrategy->resetState();
    updateView();
}

/*void fm::ui::MainWindow::saveState() {
    std::shared_ptr<util::SharedPreferences> prefs(
            util::SharedPreferences::openPreferences("ru.egor9814.fileman", "cache"));
    dirSizeStrategy->saveState(*prefs);
}

void fm::ui::MainWindow::restoreState() {
    std::shared_ptr<util::SharedPreferences> prefs(
            util::SharedPreferences::openPreferences("ru.egor9814.fileman", "cache"));
    dirSizeStrategy->restoreState(*prefs);
    updateView();
}*/

void fm::ui::MainWindow::showDirStatus(const DirSizeStrategy::DirStateType &map) {
    if (map.empty()) {
        if (!lastNothing) {
            lastNothing = true;
            currentDataViewType = data->getViewType();
            data->setViewType(DataViewType::Nothing);
        }
    } else {
        DirSizeStrategy::DirStateType result;
        for (auto &it : map.toStdMap()) {
            auto row = it.second;
            if (it.first == ".") {
                result.insert(R::string::dir_current(lang), row);
            } else if (it.first.isEmpty()) {
                result.insert(R::string::dir_no_ext(lang), row);
            } else if (it.first == "Other") {
                result.insert(R::string::dir_other(lang), row);
            } else {
                result.insert(it.first, row);
            }
        }
        if (lastNothing) {
            lastNothing = false;
            data->setViewType(currentDataViewType);
        }
        data->setData(result);
    }
}

void fm::ui::MainWindow::onAboutAction(bool) {
    QMessageBox::about(this, R::string::help_about_title(lang), R::string::help_about_text(lang));
}

void fm::ui::MainWindow::onAboutQtAction(bool) {
    QApplication::aboutQt();
}


void fm::ui::MainWindow::selectLang(QLocale::Language language) {
    if (lang.language() != language) {
        lang = QLocale(language);
        prefs->edit().putInt("lang", static_cast<qint32>(lang.language())).apply();
    }
}

void fm::ui::MainWindow::onSelectLangEn(bool) {
    selectLang(QLocale::English);
}

void fm::ui::MainWindow::onSelectLangRu(bool) {
    selectLang(QLocale::Russian);
}
