//
// Created by egor9814 on 03.05.19.
//

#include "data_loader_private.hpp"

void fm::data_loader::Loader::load()  {
    /*QMap<QString, QPair<qint64, double>> result;
    for (auto &it : dss->calculateSizes(fm::util::File(path))) {
        result.insert(it.first, QPair<qint64, double>(it.second.first, it.second.second));
    }*/
    auto result = dss->calculateSizes(fm::util::File(path));
    emit finished();
    emit loaded(result);
}