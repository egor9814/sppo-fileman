//
// Created by egor9814 on 03.05.19.
//

#ifndef FILEMAN_DATA_LOADER_PRIVATE_HPP
#define FILEMAN_DATA_LOADER_PRIVATE_HPP

#include <QtCore/QtCore>
#include <fm/dir_size_strategy.hpp>

namespace fm {

    namespace data_loader {

        class Loader : public QObject {
            Q_OBJECT

            fm::DirSizeStrategy* dss;
            QString path;

        public:
            Loader(fm::DirSizeStrategy* dss, const QString& path) : dss(dss), path(path) {}

            ~Loader() override = default;

        signals:
            void finished();
            void loaded(const DirSizeStrategy::DirStateType&);

        public slots:
            void load();
        };

    }

}

#endif //FILEMAN_DATA_LOADER_PRIVATE_HPP
