//
// Created by egor9814 on 30.04.19.
//

#ifndef __egor9814_fileman__data_view_hpp__
#define __egor9814_fileman__data_view_hpp__

#include <QtWidgets/QtWidgets>
#include <QtGui/QWindow>
#include <QtCore/QString>
#include <map>
#include <fm/dir_size_strategy.hpp>

namespace fm {

    namespace ui {

        enum class DataViewType {
            Nothing = 0, FileList, PieDiagram, BarsDiagram
        };

        struct DataGraphicsProvider {
            virtual ~DataGraphicsProvider() = default;

            virtual QWidget* getWidget() = 0;

            virtual void setData(const DirSizeStrategy::DirStateType& data) = 0;

            virtual void showLegend(Qt::Alignment) = 0;
            virtual void hideLegend() = 0;

            virtual DataViewType getType() const = 0;

            static DataGraphicsProvider* createProvider(QLocale& lang, const DataViewType& type);
        };

        class DataView : public QWidget {
            Q_OBJECT

            QGridLayout* layout;
            DataGraphicsProvider* graphicsProvider{nullptr};
            QWidget* view{nullptr};
            bool removeView{false};

            Qt::Alignment align{Qt::AlignLeft};
            bool legend{false};

            void updateLegend();

            QLocale& lang;

            DirSizeStrategy::DirStateType currentData;

        public:
            explicit DataView(QLocale& lang, QWidget* parent = nullptr);
            ~DataView() override;

            void setData(const DirSizeStrategy::DirStateType& data);

            void setViewType(const DataViewType& type);

            DataViewType getViewType() const;

            void showLegend(Qt::Alignment);

            void hideLegend();
        };

    }

}

#endif //__egor9814_fileman__data_view_hpp__
