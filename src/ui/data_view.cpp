//
// Created by egor9814 on 30.04.19.
//

#include "data_view.hpp"
#include <QtCore/QtCore>
#include <QtWidgets/QtWidgets>
#include <fm/file_util.hpp>
#include <memory>
#include "data_view/nothing.hpp"
#include "data_view/file_list.hpp"
#include "data_view/pie_diagram.hpp"
#include "data_view/bar_diagram.hpp"

fm::ui::DataGraphicsProvider *fm::ui::DataGraphicsProvider::createProvider(QLocale& lang,
        const fm::ui::DataViewType &type) {
    switch (type) {
        default:
            return nullptr;

        case DataViewType::Nothing:
            return new data_view::NothingView(lang);

        case DataViewType::FileList:
            return new data_view::FileListView(lang);

        case DataViewType::PieDiagram:
            return new data_view::PieDiagramView(lang);

        case DataViewType::BarsDiagram:
            return new data_view::BarDiagramView(lang);
    }
}


/// DataView
fm::ui::DataView::DataView(QLocale& lang, QWidget *parent) : QWidget(parent), lang(lang) {
    layout = new QGridLayout(this);
    setLayout(layout);
}

fm::ui::DataView::~DataView() {
    if (graphicsProvider) {
        if (view) {
            layout->removeWidget(view);
            if (removeView) {
                delete view;
            }
        }
        delete graphicsProvider;
    }
}

void fm::ui::DataView::setData(const DirSizeStrategy::DirStateType &data) {
    currentData = data;
    if (graphicsProvider) {
        graphicsProvider->setData(data);
    }
    updateLegend();
}

void fm::ui::DataView::setViewType(const fm::ui::DataViewType &type)  {
    if (graphicsProvider) {
        if (type == graphicsProvider->getType())
            return;

        if (view) {
            layout->removeWidget(view);
            if (removeView)
                delete view;
            view = nullptr;
        }
        delete graphicsProvider;
        graphicsProvider = nullptr;
    } else if (removeView) {
        delete view;
        view = nullptr;
        removeView = false;
    }
    graphicsProvider = DataGraphicsProvider::createProvider(lang, type);
    if (graphicsProvider) {
        view = graphicsProvider->getWidget();
        graphicsProvider->setData(currentData);
    }
    if (!view) {
        view = new QLabel("Cannot create graphic");
        removeView = true;
    } else {
        removeView = false;
    }
    layout->addWidget(view, 0, 0);
    updateLegend();
}

void fm::ui::DataView::showLegend(Qt::Alignment alignment) {
    align = alignment;
    legend = true;
    updateLegend();
}

void fm::ui::DataView::hideLegend() {
    legend = false;
    updateLegend();
}

fm::ui::DataViewType fm::ui::DataView::getViewType() const {
    if (graphicsProvider)
        return graphicsProvider->getType();
    return DataViewType::Nothing;
}

void fm::ui::DataView::updateLegend() {
    if (legend) {
        graphicsProvider->showLegend(align);
    } else {
        graphicsProvider->hideLegend();
    }
}
