//
// Created by egor9814 on 03.05.19.
//

#ifndef __egor9814_fileman__data_loader_hpp__
#define __egor9814_fileman__data_loader_hpp__

#include <QtWidgets/QtWidgets>
#include <QtCore/QtCore>
#include <fm/dir_size_strategy.hpp>

namespace fm {

    namespace ui {

        class DataLoadingDialog : public QObject {
            Q_OBJECT

            QWidget* parent;
            QLocale& lang;

        public:
            explicit DataLoadingDialog(QLocale& lang, QWidget* parent = nullptr);

            ~DataLoadingDialog();

            void load(DirSizeStrategy* dss, const QString& path);

        private slots:
            void loadFinished(const DirSizeStrategy::DirStateType &);
            //void loadCanceled();

        signals:
            void loaded(const DirSizeStrategy::DirStateType&);
        };

    }

}

#endif //__egor9814_fileman__data_loader_hpp__
