//
// Created by egor9814 on 26.04.19.
//

#ifndef __egor9814_fileman__main_window_hpp__
#define __egor9814_fileman__main_window_hpp__

#include <QtWidgets/QtWidgets>
#include <QtCore/QtCore>
#include <fm/dir_size_strategy.hpp>
#include <fm/shared_preferences.hpp>
#include <memory>
#include "data_view.hpp"
#include "data_loader.hpp"

namespace fm {

    namespace ui {

        class MainWindow : public QMainWindow {
            Q_OBJECT

            bool firstOnce{true};
            QModelIndex currentPath;

            QFileSystemModel* fileTreeModel;
            QTreeView* fileTree;

            QSplitter* mainSplitter;
            //QVBoxLayout* mainLayout;

            DataView* data;

            QShortcut* exitShortcut;
            QShortcut* fsShortcut;

            DirSizeStrategy* dirSizeStrategy;
            bool isFolderDss{true};

            DataViewType currentDataViewType;
            bool lastNothing{true};

            std::shared_ptr<fm::util::SharedPreferences> prefs;

            DataLoadingDialog* dataLoader;

            QLocale lang;

        public:
            explicit MainWindow(QWidget* parent = nullptr);
            ~MainWindow() override;

        private:
            void updateView();
            void changeDss(bool isFolder);
            void changeDataView(const DataViewType& type);
            void selectLang(QLocale::Language language);
            /*void saveState();
            void restoreState();*/

        private slots:
            void exitShortcutSlot();
            void fsShortcutSlot();

            void onDirSelected(const QModelIndex& selected, const QModelIndex& deselected);

            void onDssFolderToggled(bool checked);
            void onDssFileToggled(bool checked);

            void onViewTypeFileListToggled(bool checked);
            void onViewTypePieDiagramToggled(bool checked);
            void onViewTypeBarsToggled(bool checked);

            void onLegendNothingToggled(bool checked);
            void onLegendLeftToggled(bool checked);
            void onLegendTopToggled(bool checked);
            void onLegendRightToggled(bool checked);
            void onLegendBottomToggled(bool checked);

            void onReanalyzeAction(bool);

            void showDirStatus(const DirSizeStrategy::DirStateType &map);
            
            void onAboutAction(bool);
            void onAboutQtAction(bool);

            void onSelectLangEn(bool);
            void onSelectLangRu(bool);
        };

    }

}

#endif //__egor9814_fileman__main_window_hpp__
