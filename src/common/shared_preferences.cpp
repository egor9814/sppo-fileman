#include <utility>

//
// Created by egor9814 on 26.04.19.
//

#include <fm/shared_preferences.hpp>

/// Shared preferences impl
using SPMap = fm::util::SharedPreferences::SharedPreferencesMap;

class EditorImpl;
class SPImpl;

class EditorImpl : public fm::util::SharedPreferences::Editor {
    SPImpl* sp;
    SPMap map;

public:
    EditorImpl(SPImpl *sp, SPMap map) : sp(sp), map(std::move(map)) {}

    Editor &putMap(const QString &key, const fm::util::SharedPreferences::SharedPreferencesMap &map) override {
        for (auto &it : map) {
            this->map.insert(it);
        }
        return *this;
    }

    Editor &putBool(const QString &key, bool v) override {
        map[key] = QVariant::fromValue(v);
        return *this;
    }

    Editor &putInt(const QString &key, qint32 v) override {
        map[key] = QVariant::fromValue(v);
        return *this;
    }

    Editor &putLong(const QString &key, qint64 v) override {
        map[key] = QVariant::fromValue(v);
        return *this;
    }

    Editor &putDouble(const QString &key, double v) override {
        map[key] = QVariant::fromValue(v);
        return *this;
    }

    Editor &putString(const QString &key, QString v) override {
        map[key] = QVariant::fromValue(v);
        return *this;
    }

    fm::util::SharedPreferences &apply() override;

    Editor &clear() override {
        map.clear();
        return *this;
    }
};


char getByte(FILE* f) {
    char c = 0;
    fread(&c, 1, 1, f);
    return c;
}

template <typename T>
T unionRead(FILE* f) {
    union {
        char bytes[sizeof(T)]{0};
        T val;
    } u;
    for (size_t i = 0; i < sizeof(T); i++) {
        u.bytes[i] = getByte(f);
        if (feof(f))
            return T(0);
    }
    return u.val;
}

bool getBool(FILE* f) {
    return getByte(f) != 0;
}

short getShort(FILE* f) {
    /*short bytes[2] = {0};
    for (auto &it : bytes) {
        it = getByte(f);
        if (feof(f))
            return 0;
    }
    return bytes[0] << 8 | bytes[1];*/
    return unionRead<short>(f);
}

int getInt(FILE* f) {
    /*int bytes[2] = {0};
    for (auto &it : bytes) {
        it = getShort(f);
        if (feof(f))
            return 0;
    }
    return bytes[0] << 16 | bytes[1];*/
    return unionRead<int>(f);
}

qint64 getLong(FILE* f) {
    /*qint64 ints[2] = {0};
    for (auto &it : ints) {
        it = getInt(f);
        if (feof(f))
            return 0;
    }
    return ints[0] << 32 | ints[1];*/
    return unionRead<qint64>(f);
}

double getDouble(FILE* f) {
    /*union {
        qint64 l{0};
        double d;
    } u;
    u.l = getLong(f);
    if (feof(f))
        return 0;
    return u.d;*/
    return unionRead<double>(f);
}

QChar getChar(FILE* f) {
    return {unionRead<ushort>(f)};
}

QString getString(FILE* f) {
    auto size = getInt(f);
    if (feof(f) || !size)
        return QString("");
    QString result;
    result.resize(size);
    for (int i = 0; i < size; i++) {
        result[i] = getChar(f);
    }
    return std::move(result);
}

template <typename T>
bool addRow(SPMap& map, FILE* f, T (*func)(FILE*)) {
    auto key = getString(f);
    if (feof(f)) {
        fclose(f);
        return false;
    }
    auto value = func(f);
    map[key] = QVariant(value);
    return true;
}

void loadSP(SPMap& map, const QString& path) {
    FILE* f = fopen(path.toStdString().c_str(), "rb");
    if (!f)
        return;

    while (true) {
        char type = getByte(f);
        switch (type) {
            default:
                fclose(f);
                return;

            case 'i': {
                if (!addRow(map, f, getInt))
                    return;
                break;
            }

            case 'l': {
                if (!addRow(map, f, getLong))
                    return;
                break;
            }

            case 'd': {
                if (!addRow(map, f, getDouble))
                    return;
                break;
            }

            case 'b': {
                if (!addRow(map, f, getBool))
                    return;
                break;
            }

            case 's': {
                if (!addRow(map, f, getString))
                    return;
                break;
            }
        }
    }
}

template <typename T>
T unpack(SPMap& map, const QString& key, const T& defaultValue) {
    if (!map.count(key)) {
        map[key] = QVariant::fromValue(defaultValue);
    }
    auto &data = map[key];
    if (data.canConvert<T>()) {
        return data.value<T>();
    } else {
        return defaultValue;
    }
}

class SPImpl : public fm::util::SharedPreferences {
    fm::util::File file;
    SPMap map;

    friend class EditorImpl;

public:
    explicit SPImpl(const fm::util::File& file) : file(file) {
        ::loadSP(map, file.getAbsoluteFilePath());
    }

    bool getBool(const QString &key, bool defaultValue) override {
        return unpack(map, key, defaultValue);
    }

    qint32 getInt(const QString &key, qint32 defaultValue) override {
        return unpack(map, key, defaultValue);
    }

    qint64 getLong(const QString &key, qint64 defaultValue) override {
        return unpack(map, key, defaultValue);
    }

    double getDouble(const QString &key, double defaultValue) override {
        return unpack(map, key, defaultValue);
    }

    QString getString(const QString &key, const QString &defaultValue) override {
        return unpack(map, key, defaultValue);
    }

    const SharedPreferencesMap &getAll() override {
        return map;
    }

    Editor &edit() override {
        return *(new EditorImpl(this, map));
    }
};


void writeByte(FILE* f, char byte) {
    fwrite(&byte, 1, 1, f);
}

template <typename T>
void unionWrite(FILE* f, const T& value) {
    union {
        T val;
        char bytes[sizeof(T)]{0};
    } u;
    u.val = value;
    for (size_t i = 0; i < sizeof(T); i++) {
        writeByte(f, u.bytes[i]);
    }
}

void writeBool(FILE* f, const bool& val) {
    writeByte(f, static_cast<char>(val ? 1 : 0));
}

void writeShort(FILE* f, const short& val) {
    /*writeByte(f, static_cast<char>((val >> 8) & 0xff));
    writeByte(f, static_cast<char>(val & 0xff));*/
    unionWrite(f, val);
}

void writeInt(FILE* f, const qint32& val) {
    /*writeShort(f, static_cast<short>((val >> 16) & 0xffff));
    writeShort(f, static_cast<short>(val & 0xffff));*/
    unionWrite(f, val);
}

void writeLong(FILE* f, const qint64& val) {
    /*writeInt(f, static_cast<qint32>((val >> 32) & 0xffffffff));
    writeInt(f, static_cast<qint32>(val & 0xffffffff));*/
}

void writeDouble(FILE* f, const double& val) {
    /*union {
        double d{0};
        qint64 l;
    } u;
    u.d = val;
    writeLong(f, u.l);*/
    unionWrite(f, val);
}

void writeChar(FILE* f, const QChar& val) {
    //writeShort(f, static_cast<short>(val.unicode()));
    unionWrite(f, val.unicode());
}

void writeString(FILE* f, const QString& val) {
    writeInt(f, val.size());
    for (auto &it : val) {
        writeChar(f, it);
    }
}

template <typename T>
void writeRow(FILE* f, char type, const QString& key, const QVariant& value, void (*func)(FILE*, const T&)) {
    writeByte(f, type);
    writeString(f, key);
    func(f, value.value<T>());
}

void writeSP(const SPMap& map, const QString& path) {
    FILE* f = fopen(path.toStdString().c_str(), "wb");
    if (!f)
        return;

    for (auto &it : map) {
        const auto &val = it.second;
        if (val.type() == QVariant::Int)
            writeRow(f, 'i', it.first, it.second, writeInt);
        else if (val.type() == QVariant::LongLong)
            writeRow(f, 'l', it.first, it.second, writeLong);
        else if (val.type() == QVariant::Double)
            writeRow(f, 'd', it.first, it.second, writeDouble);
        else if (val.type() == QVariant::Bool)
            writeRow(f, 'b', it.first, it.second, writeBool);
        else if (val.type() == QVariant::String)
            writeRow(f, 's', it.first, it.second, writeString);
    }
    fclose(f);
}

fm::util::SharedPreferences &EditorImpl::apply() {
    auto &result = *sp;
    result.map = std::move(map);

    result.file.getAbsoluteParent().mkdirs();
    writeSP(result.map, result.file.getAbsoluteFilePath());

    delete this;
    return result;
}


/// Shared preferences
fm::util::SharedPreferences* fm::util::SharedPreferences::defaultPreferences(const QString &packageName) {
    return openPreferences(packageName, "default");
}

fm::util::SharedPreferences*
fm::util::SharedPreferences::openPreferences(const QString &packageName, const QString &fileName) {
    return new SPImpl(File::config() / packageName / "preferences" / fileName);
}
