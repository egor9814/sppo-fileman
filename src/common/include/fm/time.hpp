//
// Created by egor9814 on 10.05.19.
//

#ifndef __egor9814_fileman__time_hpp__
#define __egor9814_fileman__time_hpp__

#include <chrono>

namespace fm {

    namespace time {

        std::chrono::milliseconds systemMillis();

    }

}

#endif //__egor9814_fileman__time_hpp__
