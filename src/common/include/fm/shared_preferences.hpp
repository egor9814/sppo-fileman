//
// Created by egor9814 on 26.04.19.
//

#ifndef __egor9814_fileman__shared_preferences_hpp__
#define __egor9814_fileman__shared_preferences_hpp__

#include <fm/file_util.hpp>
#include <map>
#include <QtCore/QVariant>

namespace fm {

    namespace util {

        struct SharedPreferences {
            using SharedPreferencesMap = std::map<QString, QVariant>;

            static SharedPreferences* defaultPreferences(const QString& packageName);

            static SharedPreferences* openPreferences(const QString& packageName, const QString& fileName);

            struct Editor {
                virtual Editor& putMap(const QString& key, const SharedPreferencesMap& map) = 0;
                virtual Editor& putBool(const QString& key, bool v) = 0;
                virtual Editor& putInt(const QString& key, qint32 v) = 0;
                virtual Editor& putLong(const QString& key, qint64 v) = 0;
                virtual Editor& putDouble(const QString& key, double v) = 0;
                virtual Editor& putString(const QString& key, QString v) = 0;
                /*virtual Editor& put(const QString& key, QList<qint32> v) = 0;
                virtual Editor& put(const QString& key, QList<qint64> v) = 0;
                virtual Editor& put(const QString& key, QList<float> v) = 0;
                virtual Editor& put(const QString& key, QList<double> v) = 0;
                virtual Editor& put(const QString& key, QStringList v) = 0;*/
                virtual SharedPreferences& apply() = 0;
                virtual Editor& clear() = 0;
            };

            virtual Editor& edit() = 0;

            virtual bool getBool(const QString& key, bool defaultValue = false) = 0;

            virtual qint32 getInt(const QString& key, qint32 defaultValue = 0) = 0;

            virtual qint64 getLong(const QString& key, qint64 defaultValue = 0) = 0;

            virtual double getDouble(const QString& key, double defaultValue = 0) = 0;

            virtual QString getString(const QString& key, const QString& defaultValue = "") = 0;

            virtual const SharedPreferencesMap& getAll() = 0;
        };

    }

}

#endif //__egor9814_fileman__shared_preferences_hpp__
