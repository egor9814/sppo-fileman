//
// Created by egor9814 on 03.05.19.
//

#ifndef __egor9814_fileman__launch_state_hpp__
#define __egor9814_fileman__launch_state_hpp__

#include <vector>
#include <string>

namespace fm {

    namespace util {

        struct LaunchState {
            std::string execPath;
            std::string startPath;

            static const LaunchState& get();
        };
    }

}

#endif //__egor9814_fileman__launch_state_hpp__
