//
// Created by egor9814 on 26.04.19.
//

#ifndef __egor9814_fileman__file_util_hpp__
#define __egor9814_fileman__file_util_hpp__

#include <QtCore/QFile>
#include <QtCore/QFileInfo>
#include <QtCore/QFileInfoList>
#include <QtCore/QDir>
#include <QtCore/QString>
#include <list>

namespace fm {

    namespace util {

        class FileSizeUnit {
        public:
            enum Unit : unsigned int {
                Bytes = 0,
                KBytes,
                MBytes,
                GBytes,
                TBytes
            };

        private:
            double value;
            Unit unit;

        public:
            explicit FileSizeUnit(double value, Unit unit = Bytes);

            FileSizeUnit& autoDetect();

            FileSizeUnit& up();

            FileSizeUnit& down();

            double getValue() const;

            Unit getUnit() const;

            QString toString() const;
        };

        class File {
            QFileInfo info;

        public:
            static const File& home();

            static const File& root();

            static const File& config();

            static File current();

            static const QChar& fileSeparator();

            explicit File(QString path = "");

            explicit File(const QFileInfo&);

            ~File();

            QString getFileName() const;

            QString getAbsolutePath() const;

            File getAbsoluteFile() const;

            QString getAbsoluteFilePath() const;

            File getAbsoluteParent() const;

            QString getFilePath() const;

            QString getFileExtension() const;

            std::list<File> getFiles() const;

            std::list<QString> getFileNames() const;

            bool isDir() const;

            bool isFile() const;

            bool isHidden() const;

            qint64 getFileSize() const;

            FileSizeUnit getFileSizeUnit() const;

            bool exists() const;

            bool mkdir() const;

            bool mkdirs() const;

            File operator/(const QString& path) const;

            File operator/(const File& file) const;
        };

    }

}

#endif //__egor9814_fileman__file_util_hpp__
