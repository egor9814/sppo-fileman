//
// Created by egor9814 on 10.05.19.
//

#include <fm/time.hpp>
#include <src/common/include/fm/time.hpp>

std::chrono::milliseconds fm::time::systemMillis() {
    using namespace std::chrono;
    return duration_cast<milliseconds>(system_clock::now().time_since_epoch());
}
