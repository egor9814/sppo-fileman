//
// Created by egor9814 on 26.04.19.
//

#include <utility>
#include <functional>
#include <fm/file_util.hpp>

/// FileSizeUnit
fm::util::FileSizeUnit::FileSizeUnit(double value, fm::util::FileSizeUnit::Unit unit)
        : value(value), unit(unit) {}

fm::util::FileSizeUnit &fm::util::FileSizeUnit::autoDetect() {
    while (value >= 1024.0 && unit != TBytes) {
        up();
    }
    while (value < 1.0 && unit != Bytes) {
        down();
    }
    return *this;
}

fm::util::FileSizeUnit &fm::util::FileSizeUnit::up() {
    if (unit != TBytes) {
        value /= 1024.0;
        unit = (Unit)(unit + 1); // dangerous code!!!
    }
    return *this;
}

fm::util::FileSizeUnit &fm::util::FileSizeUnit::down() {
    if (unit != Bytes) {
        value *= 1024.0;
        unit = (Unit)(unit - 1); // dangerous code!!!
    }
    return *this;
}

double fm::util::FileSizeUnit::getValue() const {
    return value;
}

fm::util::FileSizeUnit::Unit fm::util::FileSizeUnit::getUnit() const {
    return unit;
}

QString fm::util::FileSizeUnit::toString() const {
    auto result = QString::number(value, 'f', 2);
    switch (unit) {
        default:
            return result;

        case Bytes:
            return result + "B";

        case KBytes:
            return result + "KB";

        case MBytes:
            return result + "MB";

        case GBytes:
            return result + "GB";

        case TBytes:
            return result + "TB";
    }
}


/// File
fm::util::File::File(QString path) : info(path) {}

fm::util::File::File(const QFileInfo &i) : info(i) {}

fm::util::File::~File() = default;


QString fm::util::File::getFileName() const {
    return info.fileName();
}

QString fm::util::File::getAbsolutePath() const {
    return info.absolutePath();
}

fm::util::File fm::util::File::getAbsoluteFile() const {
    return File(getAbsoluteFilePath());
}

QString fm::util::File::getAbsoluteFilePath() const {
    return info.absoluteFilePath();
}

fm::util::File fm::util::File::getAbsoluteParent() const {
    return File(getAbsolutePath());
}

QString fm::util::File::getFilePath() const {
    return info.filePath();
}

QString fm::util::File::getFileExtension() const {
    auto name = getFileName();
    auto index = name.lastIndexOf('.');
    if (index < 0)
        return QString();
    auto left = name.left(index);
    if (left.isEmpty())
        return QString();
    name = name.right(name.size() - index - 1);
    return name;
}

template <typename ResultType>
std::list<ResultType> fileList(const fm::util::File& file, std::function<ResultType(const QFileInfo&)>&& f) {
    std::list<ResultType> result;
    if (file.isDir()) {
        QDir dir(file.getAbsoluteFilePath());
        auto c = dir.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot
                | QDir::Hidden | QDir::System | QDir::NoSymLinks);
        for (auto &i : c) {
            result.push_back(f(i));
        }
    }
    return std::move(result);
}

std::list<fm::util::File> fm::util::File::getFiles() const {
    return fileList<File>(*this, [](const QFileInfo &i) -> File { return File(i); });
}

std::list<QString> fm::util::File::getFileNames() const {
    return fileList<QString>(*this, [](const QFileInfo &i) -> QString { return i.fileName(); });
}

bool fm::util::File::isDir() const {
    return info.isDir();
}

bool fm::util::File::isFile() const {
    return info.isFile();
}

bool fm::util::File::isHidden() const {
    return info.isHidden();
}

qint64 fm::util::File::getFileSize() const {
    return info.size();
}

fm::util::FileSizeUnit fm::util::File::getFileSizeUnit() const {
    return FileSizeUnit(info.size());
}

bool fm::util::File::exists() const {
    return info.exists();
}

bool fm::util::File::mkdir() const {
    QDir dir(getAbsolutePath());
    return dir.mkdir(getFileName());
}

bool fm::util::File::mkdirs() const {
    if (!getAbsoluteParent().exists())
        getAbsoluteParent().mkdirs();
    return mkdir();
}

fm::util::File fm::util::File::operator/(const QString &path) const {
    return File(getAbsoluteFilePath() + fileSeparator() + path);
}

fm::util::File fm::util::File::operator/(const fm::util::File &file) const {
    return File(getAbsoluteFilePath() + fileSeparator() + file.getFilePath());
}


const fm::util::File &fm::util::File::home() {
    static File f(QDir::homePath());
    return f;
}

const fm::util::File &fm::util::File::root() {
    static File f(QDir::rootPath());
    return f;
}

const fm::util::File &fm::util::File::config() {
    static auto f = home() / ".config";
    return f;
}

fm::util::File fm::util::File::current() {
    return File(QDir::currentPath());
}

const QChar &fm::util::File::fileSeparator() {
    static QChar s(QDir::separator());
    return s;
}