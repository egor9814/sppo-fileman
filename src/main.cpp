
//#define USE_CONSOLE_MODE

#include "main.hpp"

#include <iostream>
#include <fm/file_util.hpp>
#include <fm/dir_size_strategy.hpp>
#include <fm/launch_state.hpp>
#include "ui/main_window.hpp"

using namespace fm;

class LSHolder {
    util::LaunchState state;

    LSHolder() = default;
    ~LSHolder() = default;

    static LSHolder& getInstance() {
        static LSHolder holder;
        return holder;
    }

public:
    static void init(int argc, char* argv[]) {
        auto &s = getInstance().state;
        s.execPath = argv[0];
        for (int i = 1; i < argc; i++) {
            s.startPath = argv[i];
        }
    }

    static const util::LaunchState& getState() {
        return getInstance().state;
    }
};

const util::LaunchState& fm::util::LaunchState::get() {
    return LSHolder::getState();
}


#ifdef USE_CONSOLE_MODE
void printFiles(const util::File& file, int level = 0) {
    std::cout << std::string(static_cast<unsigned long>(level * 2), ' ')
              << file.getFileName().toStdString() << std::endl;
    if (file.isDir()) {
        for (auto &i : file.getFiles()) {
            printFiles(i, level + 1);
        }
    }
}

void testDir(const util::File& dir, DirSizeStrategy* strategy) {
    auto result = strategy->calculateSizes(dir);
    if (result.empty()) {
        std::cout << "Folder is empty";
    } else {
        for (auto &it : result) {
            std::cout << it.first.toStdString() << ": " << it.second.first << " (" << it.second.second << "%)" << std::endl;
        }
    }
    std::cout << std::endl;
}
#endif

using DSMPair = QPair<qint64, double>;
using DSM = QMap<QString, DSMPair>;
Q_DECLARE_METATYPE(DSMPair);
Q_DECLARE_METATYPE(DSM);

int main(int argc, char *argv[])
{
    LSHolder::init(argc, argv);

    qRegisterMetaType<QPair<qint64, double>>("QPair<qint64,double>");
    qRegisterMetaType<QMap<QString, DSMPair>>("QMap<QString,QPair<qint64,double> >");
    qRegisterMetaType<DirSizeStrategy::DirStateType>("DirSizeStrategy::DirStateType");

    NEW_APP_INSTANCE(a, argc, argv);

#ifdef USE_CONSOLE_MODE
//    printFiles(util::File::root());
    auto f = util::File::home() / "test";
    testDir(f, DirSizeStrategy::byFolder());
    testDir(f, DirSizeStrategy::byFiles());
#else
    ui::MainWindow window;
    window.show();
#endif

    return EXEC_APP();
}
